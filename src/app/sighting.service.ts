//path sighting.service.ts

import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Sighting } from './sighting';

/**
 * @author Rayane Paiva
 * @author Roger Foss
 *
 * @description
 * The sighting service.
 *
 * @export
 * @class SightingService
 */
@Injectable({
  providedIn: 'root'
})
export class SightingService {

    /**
    * @description
    * The entity URL, which can be found in the environment.
    */
    private entityUrl = environment.REST_API_URL + 'sightings/';

    /**
     * @description
     * The constructor, which retrieves the @param http.
     */
    constructor(
        private http: HttpClient
    ) { }

    /**
     * @description
     * The method that @returns {Observable<Sighting[]>} the list of sightings.
     */
    getAllSightings(): Observable<Sighting[]> {
        return this.http.get<Sighting[]>(this.entityUrl);
    }

    /**
     * @description
     * The method that receives the @param id and @returns {Observable<Sighting>} the sighting.
     */
    getSighting(id: number): Observable<Sighting> {
        return this.http.get<Sighting>(this.entityUrl + id);
    }

    /**getSightingByBirdId(id: number): Observable<Sighting>
     * @description
     * The method that receives the bird @param id and @returns {Observable<Sighting>} the sighting.
     */
    getSightingByBirdId(id: number): Observable<Sighting> {
        return this.http.get<Sighting>(this.entityUrl + 'bird/' + id);
    }

    /**
     * @description
     * The method that receives the @param sighting and @returns {Observable<Sighting>} the sighting.
     */
    AddSighting(sighting: Sighting): Observable<Sighting> {
        return this.http.post<Sighting>(this.entityUrl, sighting);
    }
}

