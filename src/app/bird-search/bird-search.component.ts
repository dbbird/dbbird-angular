import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs';
import { Bird } from '../birds';
import { BirdService } from '../bird.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bird-search',
  templateUrl: './bird-search.component.html',
  styleUrls: ['./bird-search.component.css']
})
export class BirdSearchComponent implements OnInit {

  // birds$! : Observable<Bird>;
  // private searchTerms = new Subject<string>();

  bird: Bird | undefined | any;

  constructor(
    private birdService: BirdService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

  }

  getBirdByName(birdName: string){
    //const birdName = String(this.route.snapshot.queryParamMap.get('name'));
    this.birdService.getBirdByName(birdName)
      .subscribe(bird => this.bird = bird);
  }

  // ngOnInit(): void {
  //   this.birds$ = this.searchTerms.pipe(
  //     debounceTime(300),
  //     distinctUntilChanged(),
  //     switchMap((term : string) => this.birdService.getBirdByName(term))
  //   );
  // }

  search(birdName: string): void{
    this.getBirdByName(birdName);
  }

}
