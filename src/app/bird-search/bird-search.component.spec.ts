import { waitForAsync, TestBed } from '@angular/core/testing';
import { BirdSearchComponent } from './bird-search.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { BirdService } from '../bird.service';
import { Bird } from '../birds';

describe('BirdSearchComponent', () => {

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BirdSearchComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [FormsModule, HttpClientTestingModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => 0
              }
            }
          }
        },
        {
          provide: BirdService,
          useValue: {
            getBirdById: () => {
              return {
                subscribe: (callback: (bird: Bird) => void) => {
                  callback({
                    id: 0,
                    portugueseName: 'Corvo',
                    englishName: 'Crow',
                    scientificName: 'Corvus corax',
                    predominantColor: 'black',
                    family: 'Corvidae',
                    size: 56,
                    habitat: 'woodland',
                    genus: 'Corvus',
                    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Corvus_corax_-_Corvo_-_Corneille_-_Kraai_-_Krake_-_Kr%C3%A4he_-_Crow_-_Carrion_Crow_-_2012-08-26_-_DD_01.jpg/1200px-Corvus_corax_-_Corvo_-_Corneille_-_Kraai_-_Krake_-_Kr%C3%A4he_-_Crow_-_Carrion_Crow_-_2012-08-26_-_DD_01.jpg'
                  });
                }
              };
            }
          }
        }
      ]
    }).compileComponents();
  }));

  it('should create the component', waitForAsync(() => {
    const fixture = TestBed.createComponent(BirdSearchComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));

  it('should render the box to search for a bird', waitForAsync(() => {
    const fixture = TestBed.createComponent(BirdSearchComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('input')).toBeTruthy();
  }));

});
