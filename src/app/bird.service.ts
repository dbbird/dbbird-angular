import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { catchError, Observable , of} from 'rxjs';
import { Bird } from './birds';
import { HttpClient } from '@angular/common/http';

/**
 * @author Rayane Paiva
 * @author Roger Foss
 *
 * @description
 * The bird service.
 *
 * @export
 * @class BirdService
 *
 */
@Injectable({
  providedIn: 'root'
})
export class BirdService {

  /**
   * @description
   * The entity URL, which can be found in the environment.
   */
  private entityUrl = environment.REST_API_URL + 'birds/';

  /**
   * @description
   * The constructor, which retrieves the @param http.
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * @description
   * The method that @returns {Observable<Bird[]>} the list of birds.
   */
  getBirds(): Observable<Bird[]> {
    return this.http.get<Bird[]>(this.entityUrl);
  }

  /**
   * @description
   * The method that receives the @param id and @returns {Observable<Bird>} the bird.
   *
   */
  getBirdById(id: number): Observable<Bird> {
    return this.http.get<Bird>(this.entityUrl + id);
  }

  getBirdByName(name: string): Observable<Bird>{
    return this.http.get<Bird>(`${this.entityUrl}bird/name?name=${name}`);
  }
}
