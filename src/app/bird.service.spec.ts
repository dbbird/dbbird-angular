import { inject, TestBed, waitForAsync } from '@angular/core/testing';
import { BirdService } from './bird.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

describe('BirdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BirdService]
    });
  });

  it('should create the service', waitForAsync(inject([HttpTestingController], (birdService: BirdService, http: HttpClient) => {
    expect(birdService).toBeTruthy();
  })));
});