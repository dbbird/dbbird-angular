import { Component, OnInit } from '@angular/core';
import { Bird } from '../birds';
import { BirdService } from '../bird.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Sighting } from '../sighting';
import { SightingService } from '../sighting.service';

/**
 * @author Rayane Paiva
 * @author Roger Foss
 *
 * @description
 * The bird details component.
 *
 * @export
 * @class BirdDetailsComponent
 *
 * @implements {OnInit}
 */
@Component({
  selector: 'app-bird-details',
  templateUrl: './bird-details.component.html',
  styleUrls: ['./bird-details.component.css']
})

export class BirdDetailsComponent implements OnInit {

  /**
   * @description
   * The bird that will be displayed, which is retrieved from the bird service.
   */
  bird: Bird | undefined;
  sightings: Sighting | undefined | any;

  /**
   * @description
   * The constructor, which retrieves the @param birdService and @param route.
   */
  constructor(
    private route: ActivatedRoute,
    private birdService: BirdService,
    private sightingService: SightingService,
    private location: Location
  ) { }

  /**
   * @description
   * The method that retrieves the bird by its id.
   */
  ngOnInit() {
    const routeParams = this.route.snapshot.paramMap;
    const birdIdFromRoute = Number(routeParams.get('birdId'));

    this.getBird(birdIdFromRoute);
    this.getSightingsByBirdId(birdIdFromRoute);
  }

  private getBird(birdIdFromRoute: number): void{

    this.birdService.getBirdById(birdIdFromRoute)
      .subscribe(bird => {
        this.bird = bird;
      });
  }

  private getSightingsByBirdId(birdIdFromRoute: number): void{

    this.sightingService.getSightingByBirdId(birdIdFromRoute).subscribe(
      sightings => this.sightings = sightings
    );
  }

}
