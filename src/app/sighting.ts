/**
 * @author Rayane Paiva
 * @author Roger Foss
 *
 * @description
 * The sighting model.
 *
 * @export
 * @class Sighting
 *
 */
 export interface Sighting {
    id: number;
    date: string;
    time: string;
    location: string;
    description: string;
    bird: number;
 }
