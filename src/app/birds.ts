/**
 * @author Rayane Paiva
 * @author Roger Foss
 *
 * @description
 * The bird model.
 *
 * @export
 * @class Bird
 *
 */
export interface Bird {
  id: number;
  portugueseName: string;
  englishName: string;
  scientificName: string;
  predominantColor: string;
  family: string;
  size: number;
  habitat: string;
  genus: string;
  image: string;
}
