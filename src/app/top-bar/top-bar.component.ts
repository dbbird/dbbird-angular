import { Component, OnInit } from '@angular/core';

/**
 * @author Rayane Paiva
 * @author Roger Foss
 * 
 * @description
 * The top bar component.
 * 
 * @export
 * @class TopBarComponent
 * @implements {OnInit}
 * 
 */
@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {

  /**
   * @description
   * The constructor.
   */
  constructor() { }

  /**
   * @description
   * The Angular ngOnInit() lifecycle hook.
   */
  ngOnInit(): void {
  }

}
