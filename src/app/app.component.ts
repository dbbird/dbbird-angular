import { Component } from '@angular/core';

/**
 * @author Rayane Paiva
 * @author Roger Foss
 * 
 * @description
 * The app component, which is the root component.
 * 
 * @export
 * @class AppComponent
 * 
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /**
   * @description
   * The title of the application.
   */
  title = 'Bem-te-vi Project';
}
