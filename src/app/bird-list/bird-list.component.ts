import { Component, Input, OnInit } from '@angular/core';
import { Bird } from '../birds';
import { BirdService } from '../bird.service';

/**
 * @author Rayane Paiva
 * @author Roger Foss
 *
 * @description
 * The bird list component.
 *
 * @export
 * @class BirdListComponent
 *
 * @implements {OnInit}
 */
@Component({
  selector: 'app-bird-list',
  templateUrl: './bird-list.component.html',
  styleUrls: ['./bird-list.component.css']
})
export class BirdListComponent implements OnInit {

  /**
   * @description
   * The @type {Bird[]} list of birds that will be displayed in the bird list component.
   */
  birds: Bird | undefined | any;

  /**
   * @description
   * The constructor, which retrieves the @param {BirdService} birdService.
   */
  constructor(private birdService: BirdService) {
  }

  /**
   * @description
   * The method that retrieves the list of birds.
   */
  getBirds(): void {
    this.birdService.getBirds()
      .subscribe(birds => this.birds = birds);
  }

  /**
   * @description
   * The method called when the component is initialized.
   */
  ngOnInit(): void {
    this.getBirds();
  }

}
