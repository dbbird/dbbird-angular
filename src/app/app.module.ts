import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { BirdListComponent } from './bird-list/bird-list.component';
import { BirdDetailsComponent } from './bird-details/bird-details.component';
import { BirdSearchComponent } from './bird-search/bird-search.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    BirdListComponent,
    BirdDetailsComponent,
    BirdSearchComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component: BirdListComponent },
      { path: 'detail/:birdId', component: BirdDetailsComponent },
      { path: 'search', component: BirdSearchComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
