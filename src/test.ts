// This file is required by karma.conf.js and loads recursively all the .spec and framework files

import 'zone.js/testing';
import { getTestBed } from '@angular/core/testing';
import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting
} from '@angular/platform-browser-dynamic/testing';

/**
 * @description
 * The method that initializes the test environment, which is called before the first test.
 * 
 * @param require The require: any, which is the require function.
 * @param context, which is the context: any, which is the context function.
 */
declare const require: any;

/** First, initialize the Angular testing environment. */
getTestBed().initTestEnvironment(
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting(),
);

/** Then we find all the tests. */
const context = require.context('./', true, /\.spec\.ts$/);
/** And load the modules. */
context.keys().forEach(context);
