/**
 * The environment.
 * 
 * @author Rayane Paiva
 * @author Roger Foss
 *
 */
export const environment = {
  production: true,
  REST_API_URL: 'http://localhost:8080/api/'
};
