# Projeto Bem-Te-Vi Angular

O Projeto Bem-Te-Vi Angular é o frontend da aplicação REST para Ornitólogos amadores interessados em aves. Aqui os ornitólogos podem buscar por aves no guia desenvolvido e registrar os avistamentos das mesmas. O guia tem varias informações sobre os pássaros já cadastrados.

## Executando Localmente

###### Pré-requisitos

Certifique-se de ter instalado as seguintes ferramentas: 

* NodeJs - https://nodejs.org/en/download/
* Angular CLI - https://angular.io/cli
* VS Code - https://code.visualstudio.com/download

###### Rodando o frontend

`git clone git@gitlab.com:dbbird/bemtevi-project-angular.git`

`cd bemtevi-project-angular`

`npm start` ou `npm start`

![terminal angular](https://i.imgur.com/yNvKaBu.png)



Certifique-se também de estar rodando o [backend](https://gitlab.com/dbbird/bemtevi-project-rest) da aplicação.

Após isso você deve conseguir acessar a aplicação pelo link: `http://localhost:4200/`

![Frontend angular](https://i.imgur.com/BrldudZ.png)



## Como contribuir

1.  Faça um **fork** do projeto
2. Crie uma nova branch com as suas alterações: `git checkout -b my-feature`
3. Salve as alterações e crie uma mensagem de commit contando o que voçê fez: `git commit -m "feature: my new feature"`
4. Envie as suas alterações: `git push origin my-feature`
5. Abra um Pull Request

## Licença

Este projeto esta sob a licença [MIT](https://gitlab.com/dbbird/bemtevi-project-angular/-/blob/main/LICENSE)
